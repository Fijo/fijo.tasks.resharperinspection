﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Extentions.Boolean;
using JetBrains.Annotations;
using NAnt.Core;
using NAnt.Core.Attributes;
using NAnt.Core.Tasks;

namespace Fijo.Tasks.ReSharperInspection
{
	[PublicAPI]
	[TaskName("ReSharperInspection")]
    public class ReSharperInspection : ExternalProgramBase
    {
		#region Fields
	    private const string InspectcodeExeFileName = "inspectcode.exe";
	    private string _programArguments;
	    private string _exeName;
	    private string _reportFileFullPath;
	    #endregion

		#region Properties
		#region ForTask
		[TaskAttribute("ToolDirPath", Required = true)]
		[StringValidator(AllowEmpty = false), NotNull]
		[PublicAPI]
	    public string ToolDirPath { get; set; }

		[TaskAttribute("ReportFile", Required = true)]
		[StringValidator(AllowEmpty = false), NotNull]
		[PublicAPI]
	    public string ReportFile { get; set; }

		[TaskAttribute("SolutionFile", Required = true)]
		[StringValidator(AllowEmpty = false), NotNull]
		[PublicAPI]
	    public string SolutionFile { get; set; }
		
		[TaskAttribute("ProjectFilter", Required = false)]
		[Desc("Only inspect projects that matches a given filter expression, for example ´*Test´.")]
		[PublicAPI]
	    public string ProjectFilter { get; set; }

		[TaskAttribute("SolutionWideAnalysis", Required = false)]
		[Note("default: true")]
		[PublicAPI]
	    public bool SolutionWideAnalysis { get; set; }
		
		[TaskAttribute("CustomSettingsProfile", Required = true)]
		[StringValidator(AllowEmpty = false), NotNull]
		[PublicAPI]
	    public string CustomSettingsProfile { get; set; }
		
		[TaskAttribute("SupressBuildInSettings", Required = false)]
		[Note("default: false")]
		[PublicAPI]
	    public bool SupressBuildInSettings { get; set; }
		
		[TaskAttribute("Debug", Required = false)]
		[Note("default: false")]
		[PublicAPI]
	    public bool Debug { get; set; }
		#endregion

	    #region Overrides of ExternalProgramBase
	    public override string ProgramArguments { get { return _programArguments; } }
	    public override string ExeName { get { return Path.Combine(ToolDirPath, _exeName); } set { _exeName = value; } }
	    #endregion

		[PublicAPI]
		protected bool NoSolutionWideAnalysis {get { return !SolutionWideAnalysis; } }
		#endregion

	    public ReSharperInspection() {
		    _exeName = InspectcodeExeFileName;
		    SolutionWideAnalysis = true;
	    }

	    protected override void PrepareProcess(Process process) {
		    InitPrepare();
		    BuildArguments();
            LogForPrepare();
		    base.PrepareProcess(process);
		    AdjustEnvironmentVariables(process);
	    }

		private void AdjustEnvironmentVariables(Process process) {
			var environmentVariables = process.StartInfo.EnvironmentVariables;
			if (environmentVariables.ContainsKey("TEAMCITY_FUNC_TEST_ENV_VAR")) environmentVariables.Add("TEAMCITY_FUNC_TEST_ENV_VAR", "(PseudoValue)");
		}

		private void LogForPrepare() {
			Log(Level.Verbose, "Working directory: {0}", BaseDirectory);
			Log(Level.Verbose, "Arguments: {0}", ProgramArguments);
		}

		private void InitPrepare() {
		    _reportFileFullPath = GetFullPath(ReportFile);
	    }

		#region Arguments
	    private void BuildArguments() {
		    _programArguments = GetArguments();
	    }

	    private string GetArguments() {
		    var arguments = new StringBuilder();
		    AddString(arguments, "o", _reportFileFullPath);
		    AddBool(arguments, "no-swea", NoSolutionWideAnalysis);
		    if(!string.IsNullOrEmpty(ProjectFilter))
				AddString(arguments, "project", ProjectFilter);
		    AddString(arguments, "p", CustomSettingsProfile);
		    AddBool(arguments, "no-buildin-settings", SupressBuildInSettings);
		    AddBool(arguments, "d", Debug);
		    arguments.AppendFormat(@" ""{0}""", EscapeQuote(SolutionFile));
		    return arguments.ToString();
	    }

		private void AddString(StringBuilder arguments, string paramName, string value) {
			arguments.AppendFormat(@" /{0}=""{1}""", paramName, EscapeQuote(value));
		}

		private void AddBool(StringBuilder arguments, string paramName, bool value) {
			arguments.AppendFormat(@" /{0}={1}", paramName, value.ToLowerString());
		}
		#endregion

		private string GetFullPath(string fileName) {
		    var pathScanner = new PathScanner();
		    pathScanner.Add(fileName);
		    return pathScanner.Scan().Cast<string>().FirstOrDefault() ?? Path.Combine(Environment.CurrentDirectory, fileName);
	    }

	    private string EscapeQuote(string me) {
		    return me.Replace("\"", "\\\"");
	    }
    }
}
